console.log("Hello World");




// item nos. 3 - 7

let number = parseInt(prompt("Enter a number"));
console.log("The number you provided is: " + number);

for (let count = number; count >= 0; count--){
	console.log(count);


	if(count <= 50){
		console.log("The current value is at " + count + ". Terminating the loop.");
		break;
	}
	else if (count % 10 == 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	else if (count % 5 === 0){
		console.log(count);
	}
}


	

	

// item no. 8

const original = "supercalifragilisticexpialidocious";
const run = "docious-ali-expi-istic-fragil-cali-repus";

function reverseString(str) {
    return str.split("").reverse().join("");
}


function replacer(match, p1, p2, p3, p4, p5, p6, p7, offset, string) {
  return [p7, p6, p5, p4, p3, p2, reverseString(p1)].join('-');
}

const actual = original.replace(/(\w{5})(\w{4})(\w{6})(\w{5})(\w{4})(\w{3})(\w{7})/, replacer);

console.log(original);


// item no. 9


for(let i = 0; i < original.length; i++) {
	
	if(
		original[i].toLowerCase() == "a" ||
		original[i].toLowerCase() == "e" ||
		original[i].toLowerCase() == "i" ||
		original[i].toLowerCase() == "o" ||
		original[i].toLowerCase() == "u" 
		) {
		console.log();
	} else {
		
		console.log(original[i])
	}
}

// item no. 11

console.log(run === actual);



/*Solution*/
/*
	Loops are an important part of programming language

	for loop
	while loop
	do while loop
	for in loop

	for loop
		- initial value
		- condition 
		- iteration
	While loop
		- kind of loop that takes a single condition and evaluates it.

	Do while Loop
		- similar to a normal while loop except that the block of code being loope over is guaranteed to run at least once.

 */

// counting from 1-10
// for (let i= 1; i <=10; i++){
// 	console.log(i);
// }

// // while - counting from 1-10
// let n = 1;

// while (n <= 10){
// 	console.log(n);
// 	n++;
// }

// let doritos = 100;

// do {
// 	console.log("With " + doritos + " Doritos left, I can eat.")
// 	doritos -= 20;
// } while (doritos > 10)

// console.log("My doritos are gone now.");

// // for in
// let colors = ["red", "pink", "yellow"];

// for (let x in colors){
// 	console.log(colors[x]);
// };

// let person = {
// 	name: "Tom",
// 	weight: "150",
// 	age: 40
// };

// for (let data in person){
// 	console.log("This person's " + data + " " + person[data]);
// }

// // Solution activity
// // create a prompt that will ask the user for a number

// let number = Number(prompt("Give me a number: "));
// console.log("The number you provided is :" + number + ".");

// // create a loop that will use the number by the user and count down to 0
// for (let count = number; count >= 0; count--) {

// 	// Check what is the starting value of the loop
// 	console.log(count);

// 	/*
// 		60
// 		index.js:22 The number is divisible by 10. Skipping the number.
// 		index.js:11 59
// 		index.js:11 58
// 		index.js:11 57
// 		index.js:11 56
// 		index.js:11 55
// 		index.js:28 55
// 		index.js:11 54
// 		index.js:11 53
// 		index.js:11 52
// 		index.js:11 51
// 		index.js:11 50
// 		The current value is at 50. Terminating the loop.

// 	*/

// 	// If the value provided is less than or equal to 50, terminate the loop
// 	if (count <= 50) {

// 		console.log("The current value is at " + count + ". Terminating the loop.");
// 		break;

// 	// If the value is divisible by 10, skip printing the number
// 	} else if (count % 10 === 0) {

// 		console.log("The number is divisible by 10. Skipping the number.");
// 		continue;

// 	// If the value is divisible by 5, print the number
// 	} else if (count % 5 === 0) { 

//         console.log(count);
//     }
// }

// let string = "supercalifragilisticexpialdocious";
// console.log(string);
// let filteredString = " ";

// // Create a loop that will ierate through the whole string
// for(let i=0; i < string.length; i++){
// 	// Check what is the starting value of the loop
// 	// console.log(string[i]);
	
// 	// if the current letter is being evaluated as vowel
// 	/*
// 		iteration 1:
// 			string[0].toLowerCase() == 'a'
// 			check: is "s" == "a"
// 			else: filteredString += "s"

// 		iteration 2:
// 		i is now 1
// 		string[1].toLowerCase() == "a" || "e" || "i" || "o" || "u"
// 		check: "u" == "u"
// 		continue = skip to the next iteration
// 		filteredString = "s"

// 		iteration 3:
// 		i is now 2
// 		string[2].toLowerCase() == "a" || "e" || "i" || "o" || "u"
// 		check: "p" !== "a" || "e" || "i" || "o" || "u"
// 		else: filteredString += "p"
// 		filteredString = "sp"
// 	 */
// 	if (
// 			string[i].toLowerCase() == "a" ||
// 			string[i].toLowerCase() == "e" ||
// 			string[i].toLowerCase() == "i" ||
// 			string[i].toLowerCase() == "o" ||
// 			string[i].toLowerCase() == "u" 
// 		){
// 		// continue the loop to the next letter/character in the sequence
// 			continue;
// 		// if the current letter being evaluated is not a vowel
// 	} else {
// 		// add the letter to a different variable
// 		filteredString += string[i];
// 	}
// }
// // after the loop is complete. print the filtered string without the vowels
// console.log(filteredString);























